Given(/^I go to amazon$/) do
  visit 'http://www.amazon.com'
end

When(/^I write a search term "([^"]*)" in the search box$/) do |searchTerm|
  fill_in 'twotabsearchtextbox', :with => searchTerm
  find(:xpath, '//*[@id="nav-search"]/form/div[2]/div/input').click
end

Then(/^I see all the results of that "([^"]*)" search$/) do |result|
  expect(page).to have_content(result)
end

Then(/^i get how many results its brings$/) do
  # get the value from the results string
  r = find('#s-result-count').text.tr(',','')

  # extract the total results from the string

  if result = r.match(/(\d{1})-(\d{2}) of (\d+)/)
    actualPage, totalPages, totalResults = result.captures
  end

  # convert to Integer
  puts "Total results #{totalResults.to_i} divided in #{totalPages.to_i} pages"
end
