Feature: As a user I can enter in the search term “qa testing” in the
  search box and see the results of that search on the search results page on AMAZON.com

  Scenario Outline: As a user i search "qa testing" and related words, results are displayed
    Given I go to amazon
    When I write a search term <searchTerm> in the search box
    Then I see all the results of that <result> search
    And i get how many results its brings

    Examples:
    |searchTerm   |result                                                        |
    |"qa testing" |"Software Testing: Essential Skills for First Time Testers"   |
    |"testing"    |"Agile Testing: A Practical Guide for Testers and Agile Teams"|
    |"qa"         |"Software Testing: Essential Skills for First Time Testers"   |

